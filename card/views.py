from django.shortcuts import render
from django.http import HttpResponseRedirect
import requests

# Create your views here.
def index(request):
    return HttpResponseRedirect('/card/charizard')

def get_card(request, name):
    url = "https://api.pokemontcg.io/v1/cards?name="+name
    data = requests.get(url=url).json()
    response = {'name': name, 'cards': data['cards']}
    return render(request, "card.html", response)