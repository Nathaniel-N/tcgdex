from django.conf.urls import url
from .views import index, get_card
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^(?P<name>\w+)/$', get_card, name='get_card'),
]
